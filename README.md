# Workflow:
[![Workflow](https://user-images.githubusercontent.com/71897736/111216077-f1ecf800-85a1-11eb-969b-110615127dfd.png)](https://youtu.be/xoXVWEq94F8)
## Powers of this configuration:
- Run your **Python** or **Go** files with a shortcut
- **Autoformat and Autocompletion** for code
- Support **host language servers**
- Cool **Gruvbox**  theme
- **Lightweight** Status line
- A **tree explorer**
### Plugins:
- General use:
    - [coc](https://github.com/neoclide/coc.nvim/tree/ab4f3f5797754334def047466a998b92f3076db9)
    - [vim-fugitive](https://github.com/tpope/vim-fugitive/tree/753318ef83b685f32c6bda5ae5b65b7b239a29a7)
    - [vim-autoformat](https://github.com/Chiel92/vim-autoformat/tree/2a6f931987c1cc5e5bc0c4c44f21ac9bd4c72f3b)
    - [coc-pairs](https://github.com/neoclide/coc-pairs)
    - [Gruvbox](https://github.com/morhetz/gruvbox/tree/bf2885a95efdad7bd5e4794dd0213917770d79b7)
    - [lightline.vim](https://github.com/itchyny/lightline.vim/tree/8e013f32f524157bf14ccaa87d97be3d3a7201e2)
    - [nerdtree](https://github.com/preservim/nerdtree/tree/f63fb6984f9cd07cf723c3e2e20f6ccc0aad48c2)
    - [nerdtree-git-plugin](https://github.com/Xuyuanp/nerdtree-git-plugin/tree/5fa0e3e1487b17f8a23fc2674ebde5f55ce6a816)
    - [vim-commentary](https://github.com/tpope/vim-commentary/tree/f8238d70f873969fb41bf6a6b07ca63a4c0b82b1)
- HTML:
    - [emmet-vim](https://github.com/mattn/emmet-vim/tree/1f5daf6810d205844c039a4c9efa89317e62259d)
- Python:
    - [coc-jedi](https://github.com/pappasam/coc-jedi/tree/97b01763aa21051786aba04f41ffe97a4ff0fae9)
    - [coc-pyright](https://github.com/fannheyward/coc-pyright)
- Go:
    - [vim-go](https://github.com/fatih/vim-go/tree/95c79dcdcbc7e8e9165fa7f4a6bf17c08a6bab05)
   - [coc-go](https://github.com/josa42/coc-go)

The installation instructions for the latest version are [here](https://github.com/UltiRequiem/vimrc/releases/latest).
