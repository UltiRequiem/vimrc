command! -nargs=0 Prettier :CocCommand prettier.formatFile
nnoremap <Leader>pp :Prettier<CR>
